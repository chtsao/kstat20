---
title: "About"
date: 2020-03-02T20:56:23+08:00
draft: false
---
![Dice](https://www.simplilearn.com/ice9/free_resources_article_thumb/Data-Science-vs.-Big-Data-vs.jpg)


* Curator: C. Andy Tsao.  Office: SE A411.  Tel: 3520
* Lectures: Tue. **1300**-1500, Thr. 1610-**1710** @ AE B101
Office Hours:  Mon. 12:10-13:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* TA Office Hours

  * Wed 1700~1900 何尚謙 A408 分機：3517 
  * Thu 1700~1900 劉雅涵 A408 分機：3517
  * Mon 1800~1900 陳學蒲 A414 無分機
* Prerequisites: Calculus
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)


