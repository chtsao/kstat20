---
title: "Hello"
date: 2020-03-02T20:35:16+08:00
draft: false
tags: [why]
categories: [notes]
---
![](https://www.disruptivestatic.com/wp-content/uploads/2015/05/starting-with-why.jpg)

### Why

* Stat is sexy
	* [Data Scientist: The Sexiest Job of the 21st Century](https://hbr.org/2012/10/data-scientist-the-sexiest-job-of-the-21st-century)
* [CareerCast 2018 Job Report](https://www.careercast.com/jobs-rated/2018-jobs-rated-report) : Mathematician (2), Statistician (5), Data Scientist (7), Actuary (10), Software Developer (11).
* [CareerCast 2019 Job Report](https://www.careercast.com/jobs-rated/2019-jobs-rated-report): Data Scientist (1), Statistician (2), Mathematician (8), Actuary (10), Software Developer (11).

妳已經贏在起跑點了
不論你將來是否是統計學家，一個以統計為核心的紮實資料科學訓練（有相當高的機會）可以讓你有一個經濟獨立，有成就感，更重要地, 有趣而有意義的生涯與生命。

###  What and How?
這個問題的答案就在本學期的內容中 :)
