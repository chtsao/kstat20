---
title: "Finally. Final Exam"
date: 2020-06-12T03:22:31+08:00
draft: false
tags: [admin]
---
![](https://pictures.abebooks.com/isbn/9781545385333-us-300.jpg)
## Final Exam

1. 良辰：2020 0616 (二) 1310-1440.
2. 吉地：
   * 理工一館 B101: 學號 $>410611300.$
   * 理工一館 A307: 學號 $≤410611300.$
3. 範圍：上課及習題內容。(NEW) 對照課本章節約為 Sec 23.1-23.2, 23.4; Chapter 24, Chapter 25, Chapter 26; Sec 27.1-27.2. 另外 Chapter 13, Chapter 14 內容為大數法則以及中央極限定理，是機率的頻率詮釋，常態逼近的理論基礎。
4. 其他：No cheat sheet nor mobile phone. Prepare early and Good Luck!

提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

[Practice Final](https://chtsao.gitlab.io/kstat20/vfin2019.pdf). Discussed in class 6/11 (四)

**TA office hour change**: TA 陳學莆 期末考周 (6/15)的office hours延後一個小時 (由18:00-20:00 改成 19:00-21:00)

### 科普閱讀

讀了筆記/課本後也可以讀讀這篇文章，應該可以增加一些對這學期主題的了解。

Tsao, C.A. (2016).  [超譯統計](http://jcsa.stat.org.tw/OutWeb/L_CT/Journal/Journal_Page2_Content.asp?p2=54&p3=2#)。中國統計學報；54卷2期,  P79 - 90。

私下也認識這位作者，喜歡用不同的方式，來說明一些高冷的數學/統計概念並相信這樣做有些意義。這類的事情吃力不討好，他也做了一些。至於到底做得如何，讀者才是裁判。有意見也可和我說，我會轉達 ：）