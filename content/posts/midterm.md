---
title: "Midterm"
date: 2020-04-20T15:21:49+08:00
draft: false
categories: [note, admin]
---

![](https://miro.medium.com/max/2000/1*uSxzrxRE6JnXI2FtrL3okg.png)

### 公告
* 5/4(下禮拜一) 陳學蒲TA office hours 改為 19:00-21:00 ＠ A414
* 停課通知: 因系務活動 4/23 （四）統計學 停課一次，補課時間另外安排。抱歉！

### Midterm 
- 日期: 2020 0421 (二). 時間: 1310-1440.
- 地點： 
  - 理工一館 A307: 學號 \\( \leq  410611300.\\)
  - 理工一館 B101: 學號 \\(  >  410611300. \\)
- 範圍：上課及習題內容。
- 其他：No cheatsheet nor mobiles allowed. Prepare early and Good Luck!
提早準備，固實會的，加強生疏的，弄懂原來不會的！—-考試不難，會就簡單！*練習題 <[下載](http://faculty.ndhu.edu.tw/~chtsao/ftp/share/edu/etudesA.pdf)>*

### Midterm Summary
n=72, 排除兩位缺考同學
```{R}
# Five-point summary
Min. 1st Qu.  Median  Mean   3rd Qu. Max. 
0.00  20.50   35.00   38.72  53.00   97.00 

# Class(mid>-1) mean and standard deviation
mean=38.7; sd=24.4

# Stem-and-leaf plot
  0 | 0567888
  1 | 00006688
  2 | 0001223344557889
  3 | 03355668
  4 | 01234455578
  5 | 0015678
  6 | 035667
  7 | 0
  8 | 035
  9 | 0367
```

More at [Midsum R Notebook](https://chtsao.gitlab.io/kstat20/midcal.nb.html)

